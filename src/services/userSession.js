var UserProfile = (function() {
    var full_name = "";
  
    var getName = function() {
      return full_name;    // Or pull this from cookie/localStorage
    };
  
    var setName = function(name) {
      full_name = name;     
      // Also set this in cookie/localStorage
    };
  
    return {
      getName: getName,
      setName: setName
    }
  
  })();
  
  export default UserProfile;


var userSession = (function(){
    function getNewSession(){
        var myHeaders = new Headers();
        myHeaders.append("X-LIVEAGENT-API-VERSION", "54");
        myHeaders.append("X-LIVEAGENT-AFFINITY", "null");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        let session = fetch("https://d.la1-c1cs-lo2.salesforceliveagent.com/chat/rest/System/SessionId", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));



    }

});