/* ------------------------------- new session ------------------------------ */
export async function getCountry(){
  // alert(response.json())
  fetch("https://restcountries.com/v2/name/peru")
  .then(response => response.json())
  .then(result => console.log(result[0]["name"]))
  .catch(error => console.log('error', error));

  
}

export async function newSession(){
var myHeaders = new Headers();
myHeaders.append("X-LIVEAGENT-API-VERSION", "54");
myHeaders.append("X-LIVEAGENT-AFFINITY", "null");

var requestOptions = {
  method: 'GET',
  headers: myHeaders,
  redirect: 'follow'
};

fetch("https://d.la1-c1cs-lo2.salesforceliveagent.com/chat/rest/System/SessionId", requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
}

// output for new session
// {
//     "key": "9d852987-4c6a-483f-8c1d-e1d148784ef1!1650379653425!uDYotPUgAPWBNfNOw5SyUhdVTrY=",
//     "id": "9d852987-4c6a-483f-8c1d-e1d148784ef1",
//     "clientPollTimeout": 40,
//     "affinityToken": "96621fa0"
// }

/* ----------------------------- initialise chat ---------------------------- */
export async function initialiseChat() {
    var myHeaders = new Headers();
    myHeaders.append("X-LIVEAGENT-API-VERSION", "54");
    myHeaders.append("X-LIVEAGENT-AFFINITY", "96431fa0");
    myHeaders.append("X-LIVEAGENT-SESSION-KEY", "8ee26667-25e4-4273-aed8-f79e75d63cef!1649945554263!a9nj3mvAA75sVQoTqd1g+F0gRWQ=");
    myHeaders.append("X-LIVEAGENT-SEQUENCE", "1");
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      "organizationId": "00D1r000001pVKl",
      "deploymentId": "5724J000000LIOS",
      "buttonId": "5734J000000LJgv",
      "sessionId": "8ee26667-25e4-4273-aed8-f79e75d63cef",
      "userAgent": "Lynx/2.8.8",
      "language": "en-US",
      "screenResolution": "1900x1080",
      "visitorName": "Frank Underwood",
      "prechatDetails": [],
      "prechatEntities": [],
      "receiveQueueUpdates": true,
      "isPost": true
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("https://d.la1-c1cs-lo2.salesforceliveagent.com/chat/rest/Chasitor/ChasitorInit", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
}

/* ------------------------------ send message ------------------------------ */
export async function sendMessage(){
    var myHeaders = new Headers();
    myHeaders.append("X-LIVEAGENT-API-VERSION", "54");
    myHeaders.append("X-LIVEAGENT-AFFINITY", "96431fa0");
    myHeaders.append("X-LIVEAGENT-SESSION-KEY", "8ee26667-25e4-4273-aed8-f79e75d63cef!1649945554263!a9nj3mvAA75sVQoTqd1g+F0gRWQ=");
    myHeaders.append("Content-Type", "text/plain");

    var raw = "{\"text\" : \"Hi, I need help with my TV settings.\"}";

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("https://d.la1-c1cs-lo2.salesforceliveagent.com/chat/rest/Chasitor/ChatMessage", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
}

/* ------------------------------- get message ------------------------------ */
export async function getMessage(){
    var myHeaders = new Headers();
    myHeaders.append("X-LIVEAGENT-API-VERSION", "54");
    myHeaders.append("X-LIVEAGENT-AFFINITY", "96431fa0");
    myHeaders.append("X-LIVEAGENT-SESSION-KEY", "8ee26667-25e4-4273-aed8-f79e75d63cef!1649945554263!a9nj3mvAA75sVQoTqd1g+F0gRWQ=");

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch("https://d.la1-c1cs-lo2.salesforceliveagent.com/chat/rest/System/Messages", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
}