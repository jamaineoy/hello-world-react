import React from 'react';
import App from './App';
import ReactDOM from 'react-dom';
import {newSession,getCountry, initialiseChat, sendMessage, getMessage} from './services/postmanService';

function GoApp() {
    ReactDOM.render(
      <React.StrictMode>
      <App />
      </React.StrictMode>,
      document.getElementById('root')
    );
  }
  

const HelloWorld = () => {
  
  function sayHello() {
    alert('Hello, World!');
  }
  
  return (
    <div className="App">
        <h1>HELLO WORLD</h1>
        <button onClick={sayHello}>say Hello</button>
        <br></br>
        <br></br>
        <button onClick={GoApp}>Home</button>
        <br></br>
        <br></br>
        <button onClick={getCountry}>Peru in Console</button>
        <br></br>
        <br></br>
        <button onClick={newSession}>NewSession</button>
    </div>
  );
};

export default HelloWorld;