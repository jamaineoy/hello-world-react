import logo from './logo.svg';
import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import HelloWorld from './HelloWorld'; 

function GoHelloWorld() {
  ReactDOM.render(
    <React.StrictMode>
    <HelloWorld />
    </React.StrictMode>,
    document.getElementById('root')
  );
}

function App() {

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          href="javascript:App2();"
          className="App-link"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <br></br>

        <button onClick={GoHelloWorld}>Go to Hello World</button>
      </header>
    </div>
  );
}




function App2 () {

  // return (
  //   <HelloWorld />
  // );

  return (
    <div className='App'>HelloWorld</div>
    // <button onClick="window.location.href='index.html'">index</button>
  );
}

function App3 () {
  return (
    <HelloWorld />
  );
}

// export default App2;
export default App;